import { defineConfig } from 'vite';
import fs from 'fs';
import path from 'path';
import react from '@vitejs/plugin-react-swc';
import svgr from 'vite-plugin-svgr';

import { version } from './package.json';

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    open: true,
  },
  plugins: [react(), svgr()],
  define: {
    'import.meta.env.VITE_APP_HOME': JSON.stringify('/welcome'),
    'import.meta.env.VITE_VERSION': JSON.stringify(version),
  },
  resolve: {
    alias: fs.readdirSync(path.resolve(__dirname, './src')).reduce(
      (result, srcDir) => ({
        ...result,
        [`~/${srcDir}`]: path.resolve(__dirname, `./src/${srcDir}`),
      }),
      {},
    ),
  },
});
