import type { Breakpoint } from '@mui/material/styles';

export type MatchType = 'up' | 'down' | 'between' | 'only' | 'not';

export type WidthHook = () => Breakpoint;
