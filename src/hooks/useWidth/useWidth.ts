import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import type { Breakpoint, Breakpoints } from '@mui/material/styles';

import type { MatchType, WidthHook } from './useWidth.types';

export const useWidth: WidthHook = () => {
  const theme = useTheme();
  const keys: readonly Breakpoint[] = [...theme.breakpoints.keys].reverse();

  return (
    keys.reduce((output: Breakpoint | null, key: Breakpoint) => {
      // eslint-disable-next-line react-hooks/rules-of-hooks
      const matches = useMediaQuery(theme.breakpoints.up(key));

      return !output && matches ? key : output;
    }, null) || 'xs'
  );
};

export function useWidthMatch<M extends MatchType>(
  method: M,
  ...options: Parameters<Breakpoints[M]>
) {
  const theme = useTheme();
  const { [method]: fn } = theme.breakpoints;

  return useMediaQuery(fn(...(options as [Breakpoint, Breakpoint])));
}
