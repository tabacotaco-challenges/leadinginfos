import type { FormEvent } from 'react';

export interface AuthStoreState {
  token?: string;
  username?: string;
  password?: string;

  login: (e: FormEvent<HTMLFormElement>) => Promise<void>;
  logout: () => Promise<void>;

  resetPassword: (password: string) => Promise<void>;
}
