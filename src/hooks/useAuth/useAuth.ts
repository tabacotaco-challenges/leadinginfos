import { create } from 'zustand';
import { nanoid } from 'nanoid';

import type { AuthStoreState } from './useAuth.types';

//* Zustand Store
const useAuthStore = create<AuthStoreState>((set) => ({
  token: localStorage.getItem('token') || undefined,
  username: localStorage.getItem('username') || undefined,
  password: localStorage.getItem('password') || undefined,

  login: async (e) => {
    const formdata = new FormData(e.target as HTMLFormElement);
    const token = nanoid();

    const { username, password } = Object.fromEntries(formdata.entries()) as Pick<
      AuthStoreState,
      'username' | 'password'
    >;

    e.preventDefault();
    localStorage.setItem('token', token);
    localStorage.setItem('username', username!);
    localStorage.setItem('password', password!);
    set({ token, username, password });
  },
  logout: async () => {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('password');

    set({
      token: undefined,
      username: undefined,
      password: undefined,
    });
  },
  resetPassword: async () => {},
}));

export function useAuth() {
  const { token, login, logout } = useAuthStore();

  return {
    isAuthenticated: Boolean(token),
    token,
    login,
    logout,
  };
}

export function useUserinfo() {
  const { username, password, resetPassword } = useAuthStore();

  return {
    username,
    password,
    resetPassword,
  };
}
