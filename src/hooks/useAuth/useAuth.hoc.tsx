import { Navigate } from 'react-router-dom';
import type { ComponentProps, ComponentType } from 'react';

import { useAuth } from './useAuth';

export function withUnauthenticatedRedirect(Component: ComponentType) {
  return function WithUnauthenticatedRedirect(props: ComponentProps<typeof Component>) {
    const { isAuthenticated } = useAuth();

    return isAuthenticated ? <Navigate to="/" replace /> : <Component {...props} />;
  };
}

export function withAuthenticatedRedirect(Component: ComponentType) {
  return function WithAuthenticatedRedirect(props: ComponentProps<typeof Component>) {
    const { isAuthenticated } = useAuth();

    return isAuthenticated ? <Component {...props} /> : <Navigate to="/login" replace />;
  };
}
