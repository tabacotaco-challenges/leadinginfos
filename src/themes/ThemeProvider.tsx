import * as MuiStyle from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import type { ReactNode } from 'react';

import { components } from './components';
import { ocean } from './palettes';

const theme = MuiStyle.createTheme({
  components,
  palette: ocean,
});

export default function ThemeProvider(props: { children: ReactNode }) {
  return (
    <MuiStyle.ThemeProvider theme={theme}>
      <CssBaseline />
      {props.children}
    </MuiStyle.ThemeProvider>
  );
}
