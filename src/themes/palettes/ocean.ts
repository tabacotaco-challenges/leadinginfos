import type { PaletteOptions } from '@mui/material/styles';

export const ocean: PaletteOptions = {
  mode: 'light',
  divider: 'rgba(0, 0, 0, 0.12)',
  contrastThreshold: 3,
  background: {
    paper: '#FFFFFF',
    default: '#F0F5F9',
  },
  primary: {
    light: '#8EDCE6',
    main: '#5BC2CF',
    dark: '#3A9BAB',
    contrastText: '#FFFFFF',
  },
  secondary: {
    light: '#FFDFB9',
    main: '#FFBB83',
    dark: '#FF954F',
    contrastText: '#FFFFFF',
  },
  info: {
    light: '#BFE7FF',
    main: '#7FC6FF',
    dark: '#4FAAFF',
    contrastText: '#FFFFFF',
  },
  success: {
    light: '#C5E7D9',
    main: '#7FCDA6',
    dark: '#56B586',
    contrastText: '#FFFFFF',
  },
  warning: {
    light: '#FFEDCC',
    main: '#FFD083',
    dark: '#FFAA56',
    contrastText: '#FFFFFF',
  },
  error: {
    light: '#FFC3D6',
    main: '#FF94A3',
    dark: '#FF6C83',
    contrastText: '#FFFFFF',
  },
  text: {
    primary: '#2A3137',
    secondary: 'rgba(42, 42, 42, 0.7)',
    disabled: 'rgba(42, 42, 42, 0.5)',
  },
};
