import Slide from '@mui/material/Slide';
import { ReactElement, forwardRef } from 'react';
import type { ThemeOptions } from '@mui/material/styles';
import type { TransitionProps } from '@mui/material/transitions';

export const components: ThemeOptions['components'] = {
  MuiAppBar: {
    defaultProps: {
      position: 'sticky',
    },
  },
  MuiDialog: {
    defaultProps: {
      TransitionComponent: forwardRef(
        (
          props: TransitionProps & {
            children: ReactElement;
          },
          ref: React.Ref<unknown>,
        ) => <Slide direction="up" ref={ref} {...props} />,
      ),
    },
  },
  MuiList: {
    styleOverrides: {
      root: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',

        '& > *': {
          flexGrow: 'unset !important',
        },
      },
    },
  },
  MuiListItemIcon: {
    styleOverrides: {
      root: {
        color: 'inherit',
      },
    },
  },
  MuiTextField: {
    defaultProps: {
      variant: 'outlined',
      size: 'small',
      margin: 'normal',
    },
  },
  MuiToolbar: {
    styleOverrides: {
      root: {
        gap: 8,
      },
    },
  },
};
