import CssBaseline from '@mui/material/CssBaseline';
import ReactDOM from 'react-dom/client';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import LoginIcon from '@mui/icons-material/Login';
import { BrowserRouter } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { StrictMode, lazy } from 'react';

import { App } from './containers';
import { MuiSnackbarProvider } from '~/styles';
import { ThemeProvider } from './themes';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <StrictMode>
    <BrowserRouter>
      <QueryClientProvider client={new QueryClient()}>
        <ThemeProvider>
          <MuiSnackbarProvider>
            <CssBaseline />

            <App
              color="primary"
              routers={[
                {
                  path: '/welcome',
                  title: 'Home',
                  icon: <HomeOutlinedIcon />,
                  element: lazy(() => import('./pages/welcome')),
                },
                {
                  path: '/login',
                  title: 'Login',
                  auth: false,
                  icon: <LoginIcon />,
                  element: lazy(() => import('./pages/login')),
                },
                {
                  path: '/detail',
                  title: 'Detail',
                  auth: true,
                  icon: <InfoOutlinedIcon />,
                  element: lazy(() => import('./pages/detail')),
                },
              ]}
            />
          </MuiSnackbarProvider>
        </ThemeProvider>
      </QueryClientProvider>
    </BrowserRouter>
  </StrictMode>,
);
