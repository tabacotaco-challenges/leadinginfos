import Container from '@mui/material/Container';
import { withStyles } from 'tss-react/mui';

export const FlexColumnContainer = withStyles(
  Container,
  (theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'column',
      gap: theme.spacing(1),
    },
  }),
  { name: 'FlexColumnContainer' },
);
