import type { SvgIconProps } from '@mui/material/SvgIcon';

export type BaseSvgIconProps = Omit<SvgIconProps, 'component' | 'inheritViewBox'>;
