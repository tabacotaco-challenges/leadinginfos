import SvgIcon from '@mui/material/SvgIcon';
import { withStyles } from 'tss-react/mui';

import LOGO from '~/assets/imgs/logo.svg?react';
import type * as Types from './MuiSvgIcon.types';

export const LogoIcon = withStyles(
  (props: Types.BaseSvgIconProps) => (
    <SvgIcon {...props} component={LOGO} inheritViewBox />
  ),
  (theme, { fontSize }) => ({
    root: {
      width: 'auto !important',
      fontSize:
        fontSize === 'large'
          ? theme.typography.h2.fontSize
          : fontSize === 'medium'
            ? theme.typography.h3.fontSize
            : fontSize === 'small'
              ? theme.typography.h4.fontSize
              : 'inherit',
    },
  }),
  { name: 'LogoIcon' },
);
