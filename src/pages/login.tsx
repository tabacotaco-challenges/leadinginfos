import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import LoginIcon from '@mui/icons-material/Login';
import TextField from '@mui/material/TextField';
import { Helmet } from 'react-helmet';
import { useMutation } from '@tanstack/react-query';

import { FlexColumnContainer } from '~/styles';
import { useAuth, withUnauthenticatedRedirect } from '~/hooks';

const Login = withUnauthenticatedRedirect(function Login() {
  const { login } = useAuth();

  const { mutate } = useMutation({
    mutationFn: login,
    onSuccess: () => {},
  });

  return (
    <>
      <Helmet>
        <title>LIS | Login</title>
      </Helmet>

      <FlexColumnContainer disableGutters maxWidth="xs" sx={{ margin: 'auto' }}>
        <Card component="form" onSubmit={mutate}>
          <CardHeader
            avatar={<LoginIcon fontSize="large" />}
            title="Login"
            subheader="Please login to access the system"
            titleTypographyProps={{ variant: 'h6', fontWeight: 'bolder' }}
          />

          <CardContent>
            <TextField fullWidth required label="Username" name="username" />

            <TextField
              fullWidth
              required
              type="password"
              label="Password"
              name="password"
            />
          </CardContent>

          <CardActions sx={{ justifyContent: 'center' }}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              size="large"
              fullWidth
            >
              Login
            </Button>
          </CardActions>
        </Card>
      </FlexColumnContainer>
    </>
  );
});

export default Login;
