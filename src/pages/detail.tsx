import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import Typography from '@mui/material/Typography';
import { Helmet } from 'react-helmet';

import { DetailInfoPopover } from '~/components';
import { FlexColumnContainer } from '~/styles';
import { InfoTabs } from '~/containers';
import { withAuthenticatedRedirect } from '~/hooks';

const Detail = withAuthenticatedRedirect(function Detail() {
  return (
    <>
      <Helmet>
        <title>LIS | Detail</title>
      </Helmet>

      <FlexColumnContainer disableGutters maxWidth="sm" sx={{ margin: 'auto' }}>
        <Card>
          <CardHeader
            avatar={<InfoOutlinedIcon fontSize="large" />}
            title="Detail"
            subheader="This is a detail page"
            titleTypographyProps={{ variant: 'h6', fontWeight: 'bolder' }}
          />

          <CardContent>
            <Typography variant="body1" color="textSecondary" paragraph>
              This is a detail page. You can only access this page if you are
              authenticated.
            </Typography>

            <DetailInfoPopover label="Detail Info">
              <InfoTabs />
            </DetailInfoPopover>
          </CardContent>
        </Card>
      </FlexColumnContainer>
    </>
  );
});

export default Detail;
