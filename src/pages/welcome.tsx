import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import { Helmet } from 'react-helmet';
import { NavLink } from 'react-router-dom';

import { FlexColumnContainer, LogoIcon } from '~/styles';
import { useAuth } from '~/hooks';

export default function Welcome() {
  const { isAuthenticated } = useAuth();

  return (
    <>
      <Helmet>
        <title>LIS | Welcome</title>
      </Helmet>

      <FlexColumnContainer disableGutters maxWidth="sm" sx={{ margin: 'auto' }}>
        <Card>
          <CardHeader
            avatar={<LogoIcon fontSize="medium" color="primary" />}
            title="LIS"
            subheader="Leading Information System"
            titleTypographyProps={{ variant: 'h6', fontWeight: 'bolder' }}
          />

          <CardContent>
            LIS is dedicated to global freight forwarding system. Our customers are in US,
            Europe and Asia. We have strong domain knowledge on operation and accounting
            so we do customize system for complex freight forwarder business of group
            across the world, with flexibility to implement global based system module of
            quotation, booking, shipment, consolidation, billing, profit share, customs
            filing, documentation and integration. By utilizing technologies, we always
            satisfied with our clients to overcome challenges on various logistics
            business.Our goal is always growing with clients through our best support to
            successfully handle issue of shipping space, immediate tracking, and filing
            data. Not only system integration with ERP, we provide total IT solution
            including consulting, system development, data analysis, infrastructure and
            security.
          </CardContent>

          <CardActions sx={{ justifyContent: 'center' }}>
            <Button
              variant="contained"
              color="primary"
              size="large"
              endIcon={<ArrowForwardIcon />}
              component={NavLink}
              to={isAuthenticated ? '/detail' : '/login'}
            >
              {isAuthenticated ? 'Detail' : 'Login'}
            </Button>
          </CardActions>
        </Card>
      </FlexColumnContainer>
    </>
  );
}
