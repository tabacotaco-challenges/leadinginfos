import { makeStyles } from 'tss-react/mui';
import type { AppMenuProps } from './App.types';

export const useAppStyles = makeStyles<Pick<AppMenuProps, 'open' | 'width'>>({
  name: 'App',
})((theme, { open, width }) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    marginLeft: open ? width : 0,
    height: '100vh',
    width: `calc(100% - ${open ? width : 0}px)`,
    overflow: 'hidden',

    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  content: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    height: '100%',
    overflow: 'hidden auto',
  },
}));

export const useAppMenuStyles = makeStyles<Pick<AppMenuProps, 'color' | 'width'>>({
  name: 'AppMenu',
})((theme, { color, width }) => ({
  paper: {
    background: color === 'transparent' ? color : theme.palette[color].dark,
    color: color === 'transparent' ? 'inherit' : theme.palette[color].contrastText,
    width,

    [theme.breakpoints.only('xs')]: {
      width: '100vw',
    },
  },
  list: {
    background: 'inherit',
    color: 'inherit',
    height: '100%',
  },
  header: {
    background: 'inherit',
    color: 'inherit',
  },
  footer: {
    position: 'sticky',
    marginTop: 'auto',
    background: 'inherit',
    color: 'inherit',
    bottom: 0,
    borderTop: `1px solid ${theme.palette.divider}`,
  },
}));

export const useAppHeaderStyles = makeStyles({ name: 'AppHeader' })(() => ({
  account: {
    marginLeft: 'auto',
  },
  toggle: {
    '& > [role=button]': {
      display: 'none',
    },
    '& > [role=img]': {
      display: 'block',
    },

    '&:hover': {
      '& > [role=button]': {
        display: 'block',
      },
      '& > [role=img]': {
        display: 'none',
      },
    },
  },
}));
