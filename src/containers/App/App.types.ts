import type { ElementType, ReactElement } from 'react';

export interface AppHeaderProps {
  color: 'primary' | 'secondary' | 'transparent';
  open: boolean;
  onMenuToggle: () => void;
}

export interface AppMenuProps extends Pick<AppHeaderProps, 'color' | 'open'> {
  width: number;
  onClose: () => void;

  routers: {
    path: string;
    auth?: boolean;
    title: string;
    description?: string;
    icon: ReactElement;
    element: ElementType;
  }[];
}
