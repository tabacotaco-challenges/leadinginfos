import Container from '@mui/material/Container';
import LinearProgress from '@mui/material/LinearProgress';
import { Helmet } from 'react-helmet';
import { Navigate, Outlet, Routes, Route, useLocation } from 'react-router-dom';
import { Suspense, useEffect, useState } from 'react';

import AppHeader from './App.header';
import AppMenu from './App.menu';
import { FlexColumnContainer } from '~/styles';
import { useAppStyles } from './App.styles';
import { useWidthMatch } from '~/hooks';
import type { AppMenuProps } from './App.types';

const menuWidth = 320;

export default function App({ color, routers }: Pick<AppMenuProps, 'color' | 'routers'>) {
  const [open, setOpen] = useState(false);
  const isMediumSize = useWidthMatch('up', 'md');

  const { pathname } = useLocation();
  const { classes } = useAppStyles({ open, width: menuWidth });

  useEffect(() => {
    if (isMediumSize) {
      setOpen(true);
    }
  }, [isMediumSize]);

  useEffect(() => {
    setOpen(false);
  }, [pathname]);

  return (
    <Routes>
      <Route
        element={
          <Container disableGutters className={classes.root} maxWidth={false}>
            <Helmet>
              <title>LIS</title>
            </Helmet>

            <AppMenu
              {...{ color, open, routers }}
              width={menuWidth}
              onClose={() => setOpen(false)}
            />

            <AppHeader {...{ color, open }} onMenuToggle={() => setOpen(true)} />

            <Suspense fallback={<LinearProgress color="secondary" />}>
              <FlexColumnContainer
                component="main"
                className={classes.content}
                maxWidth={false}
              >
                <Outlet />
              </FlexColumnContainer>
            </Suspense>
          </Container>
        }
      >
        {routers.map(({ path, element: PageElement }) => (
          <Route key={path} path={path} element={<PageElement />} />
        ))}
      </Route>

      <Route path="*" element={<Navigate replace to={import.meta.env.VITE_APP_HOME} />} />
    </Routes>
  );
}
