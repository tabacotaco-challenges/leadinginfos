import AppBar from '@mui/material/AppBar';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import LogoutIcon from '@mui/icons-material/Logout';
import Toolbar from '@mui/material/Toolbar';
import { ClickAwayListener } from '@mui/base/ClickAwayListener';
import { NavLink, useLocation, useNavigate } from 'react-router-dom';
import { useMutation } from '@tanstack/react-query';

import { LogoIcon } from '~/styles';
import { useAppMenuStyles } from './App.styles';
import { useAuth } from '~/hooks';
import type { AppMenuProps } from './App.types';

export default function AppMenu({ color, routers, width, open, onClose }: AppMenuProps) {
  const navigate = useNavigate();
  const { logout } = useAuth();
  const { pathname } = useLocation();
  const { isAuthenticated } = useAuth();
  const { classes } = useAppMenuStyles({ color, width });

  const { mutate } = useMutation({
    mutationFn: logout,
    onSuccess: () => navigate('/'),
  });

  return (
    <Drawer
      variant="persistent"
      anchor="left"
      open={open}
      PaperProps={{ className: classes.paper, elevation: 1 }}
    >
      {open && (
        <ClickAwayListener onClickAway={onClose}>
          <List
            className={classes.list}
            subheader={
              <>
                <ListSubheader
                  component={AppBar}
                  elevation={0}
                  className={classes.header}
                >
                  <Toolbar disableGutters>
                    <IconButton component={NavLink} to="/" color="inherit">
                      <LogoIcon fontSize="small" />
                    </IconButton>

                    <IconButton color="inherit" sx={{ ml: 'auto' }} onClick={onClose}>
                      <ArrowBackIcon />
                    </IconButton>
                  </Toolbar>
                </ListSubheader>

                <Divider />
              </>
            }
          >
            {routers.map(({ auth, path, icon, title, description }) => {
              const visible =
                (auth && isAuthenticated) ||
                (auth === false && !isAuthenticated) ||
                auth === undefined;

              return !visible ? null : (
                <ListItemButton
                  key={path}
                  component={NavLink}
                  to={path}
                  style={{ cursor: 'pointer' }}
                  {...(path.startsWith(pathname) && {
                    selected: true,
                    sx: { color: 'secondary.main', fontWeight: 'bolder' },
                  })}
                >
                  <ListItemIcon>{icon}</ListItemIcon>
                  <ListItemText
                    primary={title}
                    secondary={description}
                    primaryTypographyProps={{ fontWeight: 'inherit' }}
                  />
                </ListItemButton>
              );
            })}

            {isAuthenticated && (
              <ListItemButton className={classes.footer} onClick={() => mutate()}>
                <ListItemIcon>
                  <LogoutIcon />
                </ListItemIcon>

                <ListItemText primary="Logout" />
              </ListItemButton>
            )}
          </List>
        </ClickAwayListener>
      )}
    </Drawer>
  );
}
