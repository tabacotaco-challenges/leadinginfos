import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import AppBar from '@mui/material/AppBar';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import { LogoIcon } from '~/styles';
import { useAppHeaderStyles } from './App.styles';
import { useAuth, useUserinfo } from '~/hooks';
import type { AppHeaderProps } from './App.types';

export default function AppHeader({ color, open, onMenuToggle }: AppHeaderProps) {
  const { isAuthenticated } = useAuth();
  const { username } = useUserinfo();
  const { classes } = useAppHeaderStyles();

  return (
    <AppBar position="static" color={color}>
      <Toolbar>
        {!open && (
          <>
            <IconButton
              color="inherit"
              size="large"
              className={classes.toggle}
              onClick={onMenuToggle}
            >
              <MenuIcon role="button" />
              <LogoIcon role="img" />
            </IconButton>
          </>
        )}

        {isAuthenticated && (
          <Toolbar disableGutters className={classes.account}>
            <Avatar>
              <AccountCircleOutlinedIcon fontSize="large" />
            </Avatar>

            <Typography variant="subtitle2">Welcome,</Typography>

            <Typography variant="h6" color="secondary" fontWeight="bolder">
              {username}
            </Typography>
          </Toolbar>
        )}
      </Toolbar>
    </AppBar>
  );
}
