import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import LockResetIcon from '@mui/icons-material/LockReset';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { useEffect, useState } from 'react';
import { useMutation } from '@tanstack/react-query';
import { useSnackbar } from 'notistack';

import { CountdownMessage, ResetPassword } from '~/components';
import { useAuth, useUserinfo } from '~/hooks';
import { useStyles } from './InfoTabs.styles';

export default function InfoTabs() {
  const [activeTab, setActiveTab] = useState(0);
  const [resetting, setResetting] = useState(false);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { logout } = useAuth();
  const { username, password, resetPassword } = useUserinfo();
  const { classes } = useStyles();

  const { mutate } = useMutation({
    mutationFn: resetPassword,
    onSuccess: () =>
      enqueueSnackbar(
        <CountdownMessage
          seconds={3}
          message="Password has been reset, the system will automatically logout after {{secs}} secs."
        />,
        {
          onClose: logout,
          variant: 'success',
          autoHideDuration: 3000,
          action: (
            <Button
              variant="text"
              color="inherit"
              onClick={() => {
                closeSnackbar();
                logout();
              }}
            >
              OK
            </Button>
          ),
        },
      ),
  });

  useEffect(() => {
    setResetting(false);
  }, [activeTab]);

  return (
    <Container disableGutters maxWidth={false}>
      <Tabs
        variant="fullWidth"
        value={activeTab}
        onChange={(_, newValue) => setActiveTab(newValue)}
      >
        <Tab label="Version" />
        <Tab label="Profile" />
      </Tabs>

      {activeTab === 0 && (
        <Container maxWidth={false} className={classes.version}>
          <Typography variant="h6" color="textPrimary">
            Version:
          </Typography>
          <Typography variant="h6" color="primary" fontWeight={600}>
            {import.meta.env.VITE_VERSION}
          </Typography>
        </Container>
      )}

      {activeTab === 1 && (
        <Container maxWidth={false} className={classes.profile}>
          <TextField disabled variant="filled" label="Username" value={username} />

          {resetting ? (
            <ResetPassword onCancel={() => setResetting(false)} onConfirm={mutate} />
          ) : (
            <TextField
              disabled
              variant="filled"
              type="password"
              label="Password"
              value={password}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Tooltip title="Reset Password">
                      <IconButton onClick={() => setResetting(true)}>
                        <LockResetIcon />
                      </IconButton>
                    </Tooltip>
                  </InputAdornment>
                ),
              }}
            />
          )}
        </Container>
      )}
    </Container>
  );
}
