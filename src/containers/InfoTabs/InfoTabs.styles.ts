import { makeStyles } from 'tss-react/mui';

export const useStyles = makeStyles({ name: 'InfoTabs' })((theme) => ({
  version: {
    display: 'flex',
    justifyContent: 'center',
    gap: theme.spacing(1),
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(6),
  },
  profile: {
    display: 'flex',
    flexDirection: 'column',
    gap: theme.spacing(1),
    paddingBottom: theme.spacing(3),
  },
}));
