/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_HOME: string;
  readonly VITE_VERSION: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}

declare module '~/assets/imgs/*.svg?react' {
  const contents: import('react').ElementType;

  export default contents;
}
