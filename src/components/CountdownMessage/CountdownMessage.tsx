import { useEffect, useMemo, useState } from 'react';
import _template from 'lodash/template';

import type { CountdownMessageProps } from './CountdownMessage.types';

export default function CountdownMessage({
  seconds,
  message,
  replacedKey = 'secs',
}: CountdownMessageProps) {
  const [secs, setSecs] = useState(seconds);

  const compiled = useMemo(
    () => _template(message, { interpolate: /{{([\s\S]+?)}}/g }),
    [message],
  );

  useEffect(() => {
    const timer = setInterval(() => setSecs((secs) => Math.max(0, secs - 1)), 1000);

    return () => clearInterval(timer);
  }, []);

  return <>{compiled({ [replacedKey]: secs })}</>;
}
