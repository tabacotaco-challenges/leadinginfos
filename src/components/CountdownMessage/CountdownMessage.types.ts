export interface CountdownMessageProps {
  seconds: number;
  message: string;
  replacedKey?: string;
}
