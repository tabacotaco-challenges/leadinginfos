import type { ReactElement } from 'react';

export interface DetailInfoPopoverProps {
  children: ReactElement;
  label: string;
}
