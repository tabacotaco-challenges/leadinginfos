import Button from '@mui/material/Button';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import { useState } from 'react';

import type { DetailInfoPopoverProps } from './DetailInfoPopover.types';

export default function DetailInfoPopover({ children, label }: DetailInfoPopoverProps) {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Button
        fullWidth
        variant="outlined"
        color="primary"
        size="large"
        endIcon={<ExpandMoreIcon />}
        onClick={() => setOpen(true)}
      >
        {label}
      </Button>

      <Dialog fullWidth maxWidth="xs" open={open} onClose={() => setOpen(false)}>
        <DialogContent sx={{ p: 0 }}>{children}</DialogContent>
      </Dialog>
    </>
  );
}
