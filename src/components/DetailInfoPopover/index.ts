export { default, default as DetailInfoPopover } from './DetailInfoPopover';
export type { DetailInfoPopoverProps } from './DetailInfoPopover.types';
