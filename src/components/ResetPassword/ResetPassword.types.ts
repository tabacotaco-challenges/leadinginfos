export interface ResetPasswordProps {
  onCancel: () => void;
  onConfirm: (password: string) => void;
}
