import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import TextField from '@mui/material/TextField';
import { useState } from 'react';

import { FlexColumnContainer } from '~/styles';
import { ResetPasswordProps } from './ResetPassword.types';

export default function ResetPassword({ onCancel, onConfirm }: ResetPasswordProps) {
  const [passwords, setPasswords] = useState<[string, string]>(['', '']);

  return (
    <FlexColumnContainer disableGutters maxWidth={false}>
      <TextField
        autoFocus
        required
        variant="filled"
        type="password"
        label="New Password"
        value={passwords[0]}
        onChange={(e) => setPasswords([e.target.value, passwords[1]])}
      />

      <TextField
        required
        variant="filled"
        type="password"
        label="Confirm Password"
        value={passwords[1]}
        onChange={(e) => setPasswords([passwords[0], e.target.value])}
        {...(passwords[1] &&
          passwords[0] !== passwords[1] && {
            error: true,
            helperText: 'Passwords do not match',
          })}
      />

      <ButtonGroup fullWidth>
        <Button variant="outlined" onClick={onCancel}>
          Cancel
        </Button>

        <Button
          variant="contained"
          color="primary"
          onClick={() => onConfirm(passwords[0])}
          disabled={
            passwords.some((password) => !password.trim()) ||
            passwords[0] !== passwords[1]
          }
        >
          Reset
        </Button>
      </ButtonGroup>
    </FlexColumnContainer>
  );
}
