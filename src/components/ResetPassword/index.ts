export { default, default as ResetPassword } from './ResetPassword';
export type { ResetPasswordProps } from './ResetPassword.types';
